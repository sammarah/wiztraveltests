import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.Select;

public class wizTests extends TestBase {

    String baseUrl = "https://www.geo-travel.az/tours/tour/best-of-baku";
    elementPage elementPage;
    WebDriverWait wait ;
    JavascriptExecutor js ;
    Select dropdown ;

    /*
    * Setting up path to chrome driver
    * Changing chrome default langauge(Estonian) to English
    * Implicit wait of 10 seconds during the execution
    * Loading the URL in the Chrome Browser
    * Initializing web driver explicit wait of 15 secs.
    * */
    @BeforeSuite
    public void Setup() throws InterruptedException {
        elementPage =new elementPage();
        initialization(baseUrl);
        wait=new WebDriverWait(driver, 15);
        js = (JavascriptExecutor) driver;
        dropdown = new Select(driver.findElement(By.id("prices")));
    }

    @Test(testName = "To verify that Custpmer can reserve a slot for a Tour Package. Per Person(For 10 Pax), $400")
    public void test1() throws InterruptedException {
        driver.manage().window().maximize();
        driver.findElement(By.xpath(elementPage.fullNameField)).sendKeys("Sammar Ahmad");
        driver.findElement(By.xpath(elementPage.emailField)).sendKeys("valid@gmail.com");
        js.executeScript("window.scrollBy(0,250)");
        //driver.findElement(By.xpath(elementPage.reserveNowBtn)).click();
        dropdown.selectByValue("99");
        String text = driver.findElement(By.xpath(elementPage.priceLabel)).getText();
        System.out.println("text1:"+text);
        Assert.assertEquals(text,"$400");
        Thread.sleep(2000);

    }

    @Test(testName = "To verify that Custpmer can reserve a slot for a Tour Package. Per Person(For 9 Pax), $405")
    public void test2() throws InterruptedException {
        dropdown.selectByValue("98");
        String text = driver.findElement(By.xpath(elementPage.priceLabel)).getText();
        System.out.println("text2:"+text);
        Assert.assertEquals(text,"$405");
        Thread.sleep(2000);
    }

    @Test(testName = "To verify that Custpmer can reserve a slot for a Tour Package. Per Person(For 8 Pax), $400")
    public void test3() throws InterruptedException {
        dropdown.selectByValue("97");
        String text = driver.findElement(By.xpath(elementPage.priceLabel)).getText();
        System.out.println("text3:"+text);
        Assert.assertEquals(text,"$400");
        Thread.sleep(2000);
    }

    @Test(testName = "To verify that Custpmer can reserve a slot for a Tour Package. Per Person(For 7 Pax), $385")
    public void test4() throws InterruptedException {
        dropdown.selectByValue("96");
        String text = driver.findElement(By.xpath(elementPage.priceLabel)).getText();
        System.out.println("text4:"+text);
        Assert.assertEquals(text,"$385");
        Thread.sleep(2000);
    }

    @Test(testName = "To verify that Custpmer can reserve a slot for a Tour Package. Per Person(For 6 Pax), $360")
    public void test5() throws InterruptedException {
        dropdown.selectByValue("95");
        String text = driver.findElement(By.xpath(elementPage.priceLabel)).getText();
        System.out.println("text5:"+text);
        Assert.assertEquals(text,"$360");
        Thread.sleep(2000);
    }

    @Test(testName = "To verify that Custpmer can reserve a slot for a Tour Package. Per Person(For 5 Pax), $325")
    public void test6() throws InterruptedException {
        dropdown.selectByValue("94");
        String text = driver.findElement(By.xpath(elementPage.priceLabel)).getText();
        System.out.println("text6:"+text);
        Assert.assertEquals(text,"$325");
        Thread.sleep(2000);
    }

    @Test(testName = "To verify that Custpmer can reserve a slot for a Tour Package. Per Person(For 4 Pax), $280")
    public void test7() throws InterruptedException {
        dropdown.selectByValue("93");
        String text = driver.findElement(By.xpath(elementPage.priceLabel)).getText();
        System.out.println("text7:"+text);
        Assert.assertEquals(text,"$280");
        Thread.sleep(2000);
    }
    @Test(testName = "To verify that Custpmer can reserve a slot for a Tour Package. Per Person(For 3 Pax), $255")
    public void test8() throws InterruptedException {
        dropdown.selectByValue("92");
        String text = driver.findElement(By.xpath(elementPage.priceLabel)).getText();
        System.out.println("text8:"+text);
        Assert.assertEquals(text,"$255");
        Thread.sleep(2000);
    }

    @Test(testName = "To verify that Custpmer can reserve a slot for a Tour Package. Per Person(For 2 Pax), $230")
    public void test9() throws InterruptedException {
        dropdown.selectByValue("91");
        String text = driver.findElement(By.xpath(elementPage.priceLabel)).getText();
        System.out.println("text9:"+text);
        Assert.assertEquals(text,"$230");
        Thread.sleep(2000);
    }

    @Test(testName = "To verify that Custpmer can reserve a slot for a Tour Package. Per Person(For 1 Pax), $140")
    public void test10() throws InterruptedException {
        dropdown.selectByValue("90");
        String text = driver.findElement(By.xpath(elementPage.priceLabel)).getText();
        System.out.println("text10:"+text);
        Assert.assertEquals(text,"$140");
        Thread.sleep(2000);
    }
    @Test(testName = "To verify that Custpmer can reserve a slot for a Tour Package. Per Person(For 1 Pax), $140")
    public void test11() throws InterruptedException {
        dropdown.selectByValue("90");
        String text = driver.findElement(By.xpath(elementPage.priceLabel)).getText();
        System.out.println("text10:"+text);
        Assert.assertEquals(text,"$140");
        Thread.sleep(2000);
    }
    @Test(testName = "To verify that Custpmer can reserve a slot for a Tour Package. Per Person(For 1 Pax), $140")
    public void test12() throws InterruptedException {
        dropdown.selectByValue("90");
        String text = driver.findElement(By.xpath(elementPage.priceLabel)).getText();
        System.out.println("text10:"+text);
        Assert.assertEquals(text,"$140");
        Thread.sleep(2000);
    }
    @Test(testName = "To verify that Custpmer can reserve a slot for a Tour Package. Per Person(For 1 Pax), $140")
    public void test13() throws InterruptedException {
        dropdown.selectByValue("90");
        String text = driver.findElement(By.xpath(elementPage.priceLabel)).getText();
        System.out.println("text10:"+text);
        Assert.assertEquals(text,"$140");
        Thread.sleep(2000);
    }
    @Test(testName = "To verify that Custpmer can reserve a slot for a Tour Package. Per Person(For 1 Pax), $140")
    public void test14() throws InterruptedException {
        dropdown.selectByValue("90");
        String text = driver.findElement(By.xpath(elementPage.priceLabel)).getText();
        System.out.println("text10:"+text);
        Assert.assertEquals(text,"$140");
        Thread.sleep(2000);
    }



    @AfterSuite
    public void After(){
        TearDown();
    }


}
