import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class TestBase {

    public static WebDriver driver;
    static String pathToChromeDriver="chromedriver1.exe";


    public static void initialization(String baseURL){
        System.setProperty("webdriver.chrome.driver",pathToChromeDriver);
        ChromeOptions option = new ChromeOptions();
        option.addArguments("start-maximized");
        option.addArguments("test-type");
        option.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
        option.setExperimentalOption("useAutomationExtension", true);
        Map<String, Object> prefs = new HashMap<String, Object>();
        Map<String, Object> langs = new HashMap<String, Object>();
        langs.put("et", "en");
        prefs.put("translate", "{'enabled' : true}");
        prefs.put("translate_whitelists", langs);
        option.setExperimentalOption("prefs", prefs);
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(baseURL);

    }

    public void TearDown(){
        driver.close();
        driver.quit();
    }

}
