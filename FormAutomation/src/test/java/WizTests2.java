import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.Select;

public class WizTests2 extends TestBase {

    String baseUrl = "https://www.geo-travel.az/tours/tour/baku-khinalig-tour-north-Azerbaijan";
    elementPage elementPage;
    WebDriverWait wait ;
    JavascriptExecutor js ;
    Select dropdown ;

    /*
     * Setting up path to chrome driver
     * Changing chrome default langauge(Estonian) to English
     * Implicit wait of 10 seconds during the execution
     * Loading the URL in the Chrome Browser
     * Initializing web driver explicit wait of 15 secs.
     * */
    @BeforeSuite
    public void Setup() throws InterruptedException {
        elementPage =new elementPage();
        initialization(baseUrl);
        wait=new WebDriverWait(driver, 15);
        js = (JavascriptExecutor) driver;
        dropdown = new Select(driver.findElement(By.id("prices")));
    }

    @Test(priority = 1)
    public void test15() throws InterruptedException {
        driver.manage().window().maximize();
        js.executeScript("window.scrollBy(0,3800)");
        Thread.sleep(5000);
        driver.findElement(By.xpath(elementPage.nameField)).sendKeys("Name Surname");
        driver.findElement(By.xpath(elementPage.email_field)).sendKeys("notValid.com");
        driver.findElement(By.xpath(elementPage.commentField)).sendKeys("My Value able Comment");
        driver.findElement(By.xpath(elementPage.submitBtn)).click();
        WebElement username = driver.findElement(By.xpath(elementPage.email_field));
        String validationMessage = username.getAttribute("validationMessage");
        System.out.println("Error message="+validationMessage);
        Assert.assertEquals(validationMessage,"Please include an '@' in the email address. 'notValid.com' is missing an '@'.");
    }

    @Test(priority = 2)
    public void test16() throws InterruptedException {
        Thread.sleep(5000);
        driver.findElement(By.xpath(elementPage.email_field)).clear();
        driver.findElement(By.xpath(elementPage.email_field)).sendKeys("notValid@");
        driver.findElement(By.xpath(elementPage.submitBtn)).click();
        WebElement username = driver.findElement(By.xpath(elementPage.email_field));
        String validationMessage = username.getAttribute("validationMessage");
        System.out.println("Error message2="+validationMessage);
        Assert.assertEquals(validationMessage,"Please enter a part following '@'. 'notValid@' is incomplete.");
    }

    @Test(priority = 3)
    public void test18() throws InterruptedException {
        Thread.sleep(2000);
        driver.findElement(By.xpath(elementPage.email_field)).clear();
        driver.findElement(By.xpath(elementPage.email_field)).sendKeys("notValid.com");
        driver.findElement(By.xpath(elementPage.commentField)).clear();
        driver.findElement(By.xpath(elementPage.submitBtn)).click();
        WebElement username = driver.findElement(By.xpath(elementPage.email_field));
        String validationMessage = username.getAttribute("validationMessage");
        System.out.println("Error message2="+validationMessage);
        Assert.assertEquals(validationMessage,"Please include an '@' in the email address. 'notValid.com' is missing an '@'.");
    }
    @Test(priority = 4)
    public void test19() throws InterruptedException {
        Thread.sleep(2000);
        driver.findElement(By.xpath(elementPage.email_field)).clear();
        driver.findElement(By.xpath(elementPage.email_field)).sendKeys("notValid@");
        driver.findElement(By.xpath(elementPage.commentField)).clear();
        driver.findElement(By.xpath(elementPage.submitBtn)).click();
        WebElement username = driver.findElement(By.xpath(elementPage.email_field));
        String validationMessage = username.getAttribute("validationMessage");
        System.out.println("Error message2="+validationMessage);
        Assert.assertEquals(validationMessage,"Please enter a part following '@'. 'notValid@' is incomplete.");
    }

    @Test(priority = 5)
    public void test20() throws InterruptedException {
        Thread.sleep(2000);
        driver.findElement(By.xpath(elementPage.email_field)).clear();
        driver.findElement(By.xpath(elementPage.email_field)).sendKeys("validmail@gmail.com");
        driver.findElement(By.xpath(elementPage.commentField)).clear();
        driver.findElement(By.xpath(elementPage.submitBtn)).click();
        WebElement comment = driver.findElement(By.xpath(elementPage.commentField));
        String validationMessage = comment.getAttribute("validationMessage");
        System.out.println("Error message2="+validationMessage);
        Assert.assertEquals(validationMessage,"Please fill out this field.");
    }

    @Test(priority = 6)
    public void test17() throws InterruptedException {
        Thread.sleep(2000);
        driver.findElement(By.xpath(elementPage.email_field)).clear();
        driver.findElement(By.xpath(elementPage.email_field)).sendKeys("validmail@gmail.com");
        driver.findElement(By.xpath(elementPage.commentField)).clear();
        driver.findElement(By.xpath(elementPage.commentField)).sendKeys("My comment - travel issues");
        driver.findElement(By.xpath(elementPage.submitBtn)).click();
        js.executeScript("window.scrollBy(0,3300)");
        driver.findElement(By.xpath(elementPage.commentAddedRow)).isDisplayed();
    }

    @AfterSuite
    public void After(){
        TearDown();
    }


}
