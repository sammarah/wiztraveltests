import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class elementPage extends TestBase {

    public elementPage() {
        PageFactory.initElements(driver, this);
    }

    String toursBtn="(//button[@type=\"submit\"])[1]";

    String firstBooking="(//*[text() = 'view more'])[1]";

    String fullNameField="//*[@id=\"name\"]";

    String emailField="//*[@id=\"email\"]";

    String reserveNowBtn="(//*[@type=\"submit\"])[1]";

    String priceLabel="//*[@id=\"price\"]";

    String nameField ="//*[@id=\"id_full_name\"]";

    String email_field ="//*[@id=\"id_email\"]";

    String commentField ="//*[@name=\"comment\"]";

    String submitBtn ="//*[text() = 'Submit']";

    String commentAddedRow ="//*[contains(text(),'My comment - travel issue')]";


//
//    String contact_information_label="//*[text() = 'Contact information']";
//
//    String required_label="//*[text() = '* Required']";
//
//    String option1_checkBox="//*[text() = 'Option 1']";
//
//    String option2_checkBox="//*[text() = 'Option 2']";
//
//    String checkField_class="//*[@class=\"appsMaterialWizToggleRadiogroupEl exportToggleEl isCheckedNext isChecked\"]";
//
//    String submit_btn="//*[text() = 'Send away']";
//
//    String deselect_btn="//*[@class=\"freebirdWidgetsDeselectableradioButton\"]";
//
//    String unSelected_options_class="//*[@class=\"appsMaterialWizToggleRadiogroupEl exportToggleEl isCheckedNext\"]";
//
//    String name_textfield="(//*[@class=\"quantumWizTextinputPaperinputInput exportInput\"])[1]";
//
//    String email_textfield="(//*[@class=\"quantumWizTextinputPaperinputInput exportInput\"])[2]";
//
//    String phoneNo_textfield="(//*[@class=\"quantumWizTextinputPaperinputInput exportInput\"])[3]";
//
//    String address_textfield="(//*[@class=\"quantumWizTextinputPapertextareaInput exportTextarea\"])[1]";
//
//    String comments_textfield="(//*[@class=\"quantumWizTextinputPapertextareaInput exportTextarea\"])[2]";
//
//    String error_highlighted_area="//*[@class=\"freebirdFormviewerComponentsQuestionBaseRoot hasError\"]";
//
//    String errorMsg_label="//*[text() = 'This is a mandatory question']";
//
//    String invalid_email_errorMsg_label="//*[text() = 'Please enter a valid email address']";
//
//    String email_label="//*[text() = 'Email ']";
//
//    String submission_success_label="//*[text() = 'Thanks for submitting your contact info!']";


}
